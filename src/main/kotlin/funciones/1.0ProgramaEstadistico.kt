package funciones

import java.util.Scanner

//
//Ara que ja sabem de què va la Covid, és interessant desenvolupar una aplicació de dades estadístiques de casos d'una malaltia X.
//Pel que, a partir d'una entrada indeterminada de números, heu de realitzar les següents funcions:
//- mostrarMenu -> que mostrarà el menú per a que l'usuari pugui triar què vol realitzar (que seran les opcions de les pròpies funcions)
//- qLectures -> retornarà el número de lectures
//- qCasosTotals -> retornarà el número de casos totals
//- darreraTaxa -> retornarà la diferència (en percentatge) entre les dues darreres lectures
//para una entrada indeterminadad de numeros tenemos que integras las sguientes soluciones
// 70 215 37 98 112 209 306 309 84 103 143 148 205 182 136 99 82 56
val scanner = Scanner(System.`in`)

fun main(args: Array<String>){
   val pparams = args
    showMenu(pparams)
}


fun showMenu(args: Array<String>) {
    val args = args
    do {
        var exit = false
    println("MENÚ:")
    println("     1- Lecturas")
    println("     2- Casos totales")
    println("     3- Diferencia en porcentaje de las dos ultimas lecturas")
    println("     4- Ajuda")
    println("     5- Sortir")
    do {
        print("Escull una opció vàlida: ")
        var opcioUsuari = scanner.next()
        when (opcioUsuari) {
            "1" -> {
                println()
                lecturas(args)
            }

            "2" -> {
                println()
                casosTotales(args)
            }

            "3" -> {
                println()
                diferencia(args)
            }

            "4" -> {
                println()
                ajuda()
            }

            "5" -> {
                println()
                println("Fins aviat!")
                exit=true
            }

            else -> opcioUsuari = "error"
        }
    } while (opcioUsuari == "error")
    println()
    }while (!exit)
}

fun lecturas(args: Array<String>){
    println("${args.size}")
}
fun casosTotales(args: Array<String>){
    var casos = 0
    for (i in args.indices){
        casos += args[i].toInt()

}
    println(casos)
}
fun diferencia(args: Array<String>){
var x = args[args.size-1].toDouble()-args[args.size-2].toDouble()
    var casos = 0.0
    for (i in args.indices){
        casos += args[i].toDouble()

    }
    var pc = (x*100)/casos
    println(pc)

}
fun ajuda(){
    println("Salvese quien pueda")
}
