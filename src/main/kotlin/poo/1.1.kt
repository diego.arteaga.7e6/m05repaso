package poo

import clases.Bebida
import clases.Pasta

/*
4.1 Pastisseria
Volem desenvolupar una petita aplicació que pugui controlar la brioixeria que comercialitza, de tal forma que de cada pasta es tingui el nom, el preu, el pes i les calories aproximades.
Crea la classe pasta per guardar aquesta informació.
Fes un petit programa que crei 3 elements (croissant, ensaimada i donut, amb els preus, pes i calories que vosaltres vulgueu) i els mostri per pantalla.

*/

fun main() {
    var donut = Pasta("Donut",1.20, 0.2, 2400,arrayOf("Harina de trigo", "Huevo", "Agua", "Sal"))
    var coca = Pasta("Coca", 4.2,0.5, 100, arrayOf("Harina de trigo", "Huevo", "Agua", "Sal"))
    var pastis = Pasta("Pastis",6.8 , 0.7, 160, arrayOf("Harina de trigo", "Huevo", "Agua", "Sal"))


    var te = Bebida("te", 1.4, true)
    var cafe = Bebida("cafe", 1.4, true)
    var aigua = Bebida("aigua", 0.9, false)
    var cerveça = Bebida("cerça", 2.4, false)

    te.mostrar()
    aigua.mostrar()

    donut.mostrar()



}