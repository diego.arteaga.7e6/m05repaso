package listas

fun main(){
    val reset = "\u001b[39m"
    val black = "\u001b[30m"
    val red = "\u001b[31m"
    val green = "\u001b[32m"
    val yellow = "\u001b[33m"
    val blue = "\u001b[34m"
    val magenta = "\u001b[35m"
    val cyan = "\u001b[36m"


    val asc = Array(1000) { (0..6).random() }
    var rt = arrayListOf<Int>(0,0,0,0,0,0)


    for (i in asc.indices) {
        when (asc[i]) {
            1 -> rt[0]++
            2 -> rt[1]++
            3 -> rt[2]++
            4 -> rt[3]++
            5 -> rt[4]++
            6 -> rt[5]++
        }

    }
    println(
        "El numero ${blue}1${reset} ha salido ${green} ${rt[0]} ${reset}veces\nEl numero ${blue}2${reset} ha salido ${green} ${rt[1]} ${reset}veces \nEl numero ${blue}3${reset} ha salido ${green}${rt[2]}${reset} veces \n" +
                "El numero ${blue}4${reset} ha salido ${green} ${rt[3]} ${reset}veces \nEl numero ${blue}5${reset} ha salido ${green} ${rt[4]}${reset}veces \nEl numero ${blue}6${reset} ha salido ${green}${rt[5]}${reset} veces \n"
    )
    var nMayor = 0
    var ind = 0
    for (i in rt.indices ){
        if (rt[i]>nMayor){
            nMayor = rt[i]
            ind = rt.indexOf(rt[i])
        }
    }

    println("El numero ${ind + 1} ha salido mas veces")

}
