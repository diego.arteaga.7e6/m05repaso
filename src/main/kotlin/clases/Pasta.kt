package clases

class Pasta (val nom: String, val preu: Double, val pes: Double, val cal: Int, val ing: Array<String>){

    fun mostrar(){
        println("${this.nom},${this.pes}, ${this.cal}\n" +
                "Els ingredients son:")
    for (i in this.ing.indices){
        println("${this.ing[i]}")
    }
    }

}